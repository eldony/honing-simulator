export class Hone{
    /**
     * runs a single honing trial from the start to desired level
     * costs: dictionary of material costs
     * baseProbability: list of honing success chance across levels
     */
    constructor(from, to, costs, baseProbability){
        this.from = from; // item initial enhancement level
        this.to = to; // item desired enhancement level
        this.costs = costs;
        this.baseProbability = baseProbability;
        this.output = {
            silver:0,
            gold:0,
            shards:0,
            fusion:0,
            crystals:0,
            leapstones:0,
            fails:0,
            pity:0,
        }
        this.materials = ["silver", "gold", "shards", "fusion", "crystals", "leapstones"];
        this.materialsFlat = ["silverFlat", "shardsFlat"];
        this.artisanEnergy = 0;
        this.currentLevel = this.from;
        this.currentProbability = this.baseProbability[this.currentLevel];
    }

    /**
     * One honing attempt
     */
    oneTap(){
        if (this.currentLevel >= this.to){
            // failsafe to prevent index error
            return;
        }

        if (this.artisanEnergy >= 1){
            this.output["pity"] += 1;
            this.honeSucceed();
            return;
        }

        let rand;
        rand = Math.random();

        if (rand < this.currentProbability){
            this.honeSucceed();
        }

        else{
            this.honeFail();
        }
    }
    
    honeSucceed(){
        this.spendMaterialsFlat(); // add the flat costs (once per honing success)
        this.spendMaterials(); // add the costs for current level
        this.currentLevel += 1;
        this.artisanEnergy = 0; // reset artisan energy
        if (this.currentLevel < this.to){ // initialise probability to base probability of next level
            this.currentProbability = this.baseProbability[this.currentLevel];
        }
    }

    honeFail(){
        this.spendMaterials(); // add the costs for current level
        this.artisanEnergy += 0.46*this.currentProbability; // https://www.inven.co.kr/board/lostark/4821/73836
        this.currentProbability += this.baseProbability[this.currentLevel]*0.1; // 10% of base chance added per fail
        this.output["fails"] += 1;
    }

    /**
     * Add materials spent to output
     */
    spendMaterials(){
        this.materials.forEach(mats => {
            this.output[mats] += this.costs[mats][this.currentLevel];
        });
    }

    /**
     * Add flat costs of materials spent to output
     */
    spendMaterialsFlat(){
        this.materialsFlat.forEach(matsFlat => {
            let mats = matsFlat.slice(0,-4);
            this.output[mats] += this.costs[matsFlat][this.currentLevel];
        });
    }

    honeUntilDesiredLevel(){
        while (this.currentLevel < this.to){
            this.oneTap();
        }
    }
}

export function printObject(obj){
    for (let key in obj){
        console.log(key+":",obj[key]);
    }
}

// let h = new HoneSampler(0,15,"armor1302", 1000, "yes");
// let g = new HoneSampler
// h.honeUntilDesiredLevel();
