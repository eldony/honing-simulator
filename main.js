import { HoneSampler } from "./HoneSampler.js";
export function main(from, to, itemType, samples, researchPercentage){
    if (!validateInput(from, to, samples)){return;}
    let h = new HoneSampler(from, to, itemType, samples, researchPercentage);
    h.simulateNTimes();
    h.getPercentiles();

    let text = "<table>" +
        "<tr> <th>Type</th> <th>Median</th> <th>75th Percentile</th> <th>95th Percentile</th> </tr>"

    for (let key in h.percentiles){
        text += `<tr> <td>${key}</td>`
        h.percentiles[key].forEach(element => {
            text += `<td> ${element} </td>`;
        });
        text += "</tr>"
    }
    text += "</table>"
    document.getElementById("results").innerHTML = text;
}

export function validateInput(from, to, samples){
    let valid = 0;
    try{
        let f = parseInt(from);
        let t = parseInt(to);
        let s = parseInt(samples);

        if (f >= 0 && f <15){ valid += 1;}
        if (t > 0 && t <= 15){ valid += 1;}
        if (s > 0){ valid += 1;}
        if (t > f){ valid += 1;}
    }

    catch(err){
        document.getElementById("results").innerHTML = "Invalid Input"
    }

    if (valid < 4){
        document.getElementById("results").innerHTML = "Invalid Input";
        return false;
    }

    return true;    

}

document.getElementById("samples").defaultValue = "999999"; 
let myInput = document.getElementById("button")
myInput.addEventListener('click', function(){
    let from = document.getElementById("from").value;
    let to = document.getElementById("to").value;
    let samples = document.getElementById("samples").value;
    let itemType = document.getElementById("itemtype").value;
    let researchBonus = document.getElementById("research").value;
    main(from, to, itemType, samples, researchBonus);
})