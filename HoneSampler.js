import { Hone } from  "./Hone.js"
import { costs } from "./cost.js";
import { probs } from "./probs.js";

export class HoneSampler{
    constructor(from, to, itemType, n, researchBonus){
        this.n = parseInt(n);
        this.from = parseInt(from);
        this.to = parseInt(to);
        this.researchBonus = parseFloat(researchBonus);
        this.costs = structuredClone(costs[itemType]);
        this.data = {
            silver:[],
            gold:[],
            shards:[],
            fusion:[],
            crystals:[],
            leapstones:[],
            fails:[],
            pity:[],
        }
        this.materials = ["silver", "gold", "shards", "fusion", "crystals", "leapstones"];
        this.materialsFlat = ["silverFlat", "shardsFlat"];
        let s = itemType.slice(-4);
        if (s=="1302"||s=="1340"){
            this.baseProbability = structuredClone(probs["t3"]);
        }
        else{
            this.baseProbability = structuredClone(probs["t2"]);
        }
        this.applyResearchBonus();
    }

    simulateOnce(){
        let h = new Hone(this.from, this.to, this.costs, this.baseProbability);
        h.honeUntilDesiredLevel();
        for (let key in h.output){
            this.data[key].push(h.output[key]);
        }
    }

    simulateNTimes(){
        for (let i=0; i<this.n; i++){
            this.simulateOnce();
        }
    }

    /**
     * returns the 50th 75th and 95th percentiles of simulation in an array
     */
    getPercentiles(){
        // sort simulation data
        for (let key in this.data){
            this.data[key] = this.data[key].sort(function(a,b) {return a - b;});
        }
        // calculate percentiles
        let p = [0.5, 0.75, 0.95]
        let percentiles = [];
        let output = {};
        let i;
        p.forEach(x => {
            i = Math.floor(x*this.n);
            for (let key in this.data){
                if (!output[key]){
                    output[key] = [];
                }
                output[key].push(this.data[key][i]);
            }
        });
        this.percentiles = output;
        return output
    }

    applyResearchBonus(){
        // increase honing chance by research rate
        if (this.researchBonus==0){
            return;
        }

        for (let i=0; i<this.baseProbability.length; i++){
            this.baseProbability[i] += this.researchBonus;
        }
        // discount flat costs by research percentage
        this.materialsFlat.forEach(element => {
            for (let i=0; i<this.costs[element].length; i++){
                this.costs[element][i] = Math.ceil(this.costs[element][i]*(1-this.researchBonus));
            }
        });

    }
}